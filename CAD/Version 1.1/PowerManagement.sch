EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Li-ion_Thesis_Symbol_Library:AZ1117-3.3 U?
U 1 1 5F185E90
P 2175 1225
AR Path="/5F185E90" Ref="U?"  Part="1" 
AR Path="/5F183216/5F185E90" Ref="U3"  Part="1" 
F 0 "U3" H 2300 1649 50  0000 C CNN
F 1 "AZ1117-3.3" H 2300 1558 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Voltage_Regulator_SOT-223" H 2175 1475 50  0001 C CIN
F 3 "https://www.diodes.com/assets/Datasheets/AZ1117.pdf" H 2175 1225 50  0001 C CNN
F 4 "AZ1117IH-3.3TRG1" H 2175 1575 50  0001 C CNN "Manufacturer Part Number"
F 5 "AZ1117IH-3.3TRG1DICT-ND" H 2300 1467 50  0000 C CNN "Digi-Key Part Number"
F 6 "0.493" H 2300 1376 50  0000 C CNN "Cost (A$)"
	1    2175 1225
	1    0    0    -1  
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J?
U 1 1 5F185E96
P 2825 3050
AR Path="/5F185E96" Ref="J?"  Part="1" 
AR Path="/5F183216/5F185E96" Ref="J7"  Part="1" 
F 0 "J7" H 2905 3042 50  0000 L CNN
F 1 "Power Input" H 2905 2951 50  0000 L CNN
F 2 "Li-ion_Thesis_Footprint_Library:1984976_HV_Terminal_2_Way" H 2825 3050 50  0001 C CNN
F 3 "~" H 2825 3050 50  0001 C CNN
F 4 "277-6354-ND" H 2825 3050 50  0001 C CNN "Digi-Key Part Number"
	1    2825 3050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F185E9C
P 2625 3050
AR Path="/5F185E9C" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F185E9C" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 2625 2900 50  0001 C CNN
F 1 "+5V" H 2640 3223 50  0000 C CNN
F 2 "" H 2625 3050 50  0001 C CNN
F 3 "" H 2625 3050 50  0001 C CNN
	1    2625 3050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F185EA2
P 2625 3150
AR Path="/5F185EA2" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F185EA2" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 2625 2900 50  0001 C CNN
F 1 "GND" H 2630 2977 50  0000 C CNN
F 2 "" H 2625 3150 50  0001 C CNN
F 3 "" H 2625 3150 50  0001 C CNN
	1    2625 3150
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F185EA8
P 1600 1225
AR Path="/5F185EA8" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F185EA8" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 1600 1075 50  0001 C CNN
F 1 "+5V" H 1525 1375 50  0000 L CNN
F 2 "" H 1600 1225 50  0001 C CNN
F 3 "" H 1600 1225 50  0001 C CNN
	1    1600 1225
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5F185EAE
P 3025 1300
AR Path="/5F185EAE" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F185EAE" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 3025 1150 50  0001 C CNN
F 1 "+3V3" H 2925 1475 50  0000 L CNN
F 2 "" H 3025 1300 50  0001 C CNN
F 3 "" H 3025 1300 50  0001 C CNN
	1    3025 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2725 1225 2775 1225
Wire Wire Line
	2775 1225 2775 1300
Wire Wire Line
	2725 1375 2775 1375
Wire Wire Line
	2775 1375 2775 1300
Connection ~ 2775 1300
$Comp
L Device:C C?
U 1 1 5F185EB9
P 3025 1450
AR Path="/5F185EB9" Ref="C?"  Part="1" 
AR Path="/5F183216/5F185EB9" Ref="C26"  Part="1" 
F 0 "C26" H 3140 1496 50  0000 L CNN
F 1 "2.2uF" H 3140 1405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3063 1300 50  0001 C CNN
F 3 "~" H 3025 1450 50  0001 C CNN
F 4 "587-1430-1-ND" H 3025 1450 50  0001 C CNN "Digi-Key Part Number"
	1    3025 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F185EBF
P 1600 1375
AR Path="/5F185EBF" Ref="C?"  Part="1" 
AR Path="/5F183216/5F185EBF" Ref="C25"  Part="1" 
F 0 "C25" H 1715 1421 50  0000 L CNN
F 1 "2.2uF" H 1715 1330 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1638 1225 50  0001 C CNN
F 3 "~" H 1600 1375 50  0001 C CNN
F 4 "587-1430-1-ND" H 1600 1375 50  0001 C CNN "Digi-Key Part Number"
	1    1600 1375
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F185EC5
P 2175 1800
AR Path="/5F185EC5" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F185EC5" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 2175 1550 50  0001 C CNN
F 1 "GND" H 2180 1627 50  0000 C CNN
F 2 "" H 2175 1800 50  0001 C CNN
F 3 "" H 2175 1800 50  0001 C CNN
	1    2175 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1875 1225 1600 1225
Connection ~ 1600 1225
Wire Wire Line
	2775 1300 3025 1300
Connection ~ 3025 1300
Wire Wire Line
	2175 1525 2175 1650
Wire Wire Line
	1600 1525 1600 1650
Wire Wire Line
	1600 1650 2175 1650
Connection ~ 2175 1650
Wire Wire Line
	2175 1650 2175 1800
Wire Wire Line
	3025 1600 3025 1650
Wire Wire Line
	3025 1650 2175 1650
Text GLabel 1950 3100 2    50   Input ~ 0
USB_P
Text GLabel 1950 3000 2    50   Input ~ 0
USB_N
$Comp
L dk_USB-DVI-HDMI-Connectors:10118193-0001LF J?
U 1 1 5F185EE1
P 1575 3100
AR Path="/5F185EE1" Ref="J?"  Part="1" 
AR Path="/5F183216/5F185EE1" Ref="J6"  Part="1" 
F 0 "J6" H 1639 3845 60  0000 C CNN
F 1 "10118193-0001LF" H 1639 3739 60  0000 C CNN
F 2 "digikey-footprints:USB_Micro_B_Female_10118193-0001LF" H 1775 3300 60  0001 L CNN
F 3 "http://www.amphenol-icc.com/media/wysiwyg/files/drawing/10118193.pdf" H 1775 3400 60  0001 L CNN
F 4 "609-4616-1-ND" H 1775 3500 60  0001 L CNN "Digi-Key_PN"
F 5 "10118193-0001LF" H 1775 3600 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 1775 3700 60  0001 L CNN "Category"
F 7 "USB, DVI, HDMI Connectors" H 1775 3800 60  0001 L CNN "Family"
F 8 "http://www.amphenol-icc.com/media/wysiwyg/files/drawing/10118193.pdf" H 1775 3900 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/amphenol-icc-fci/10118193-0001LF/609-4616-1-ND/2785380" H 1775 4000 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN RCPT USB2.0 MICRO B SMD R/A" H 1775 4100 60  0001 L CNN "Description"
F 11 "Amphenol ICC (FCI)" H 1775 4200 60  0001 L CNN "Manufacturer"
F 12 "Active" H 1775 4300 60  0001 L CNN "Status"
F 13 "609-4616-1-ND" H 1575 3100 50  0001 C CNN "Digi-Key Part Number"
	1    1575 3100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F185EE7
P 1475 3700
AR Path="/5F185EE7" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F185EE7" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 1475 3450 50  0001 C CNN
F 1 "GND" H 1480 3527 50  0000 C CNN
F 2 "" H 1475 3700 50  0001 C CNN
F 3 "" H 1475 3700 50  0001 C CNN
	1    1475 3700
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F185EED
P 1975 2900
AR Path="/5F185EED" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F185EED" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 1975 2750 50  0001 C CNN
F 1 "+5V" H 1990 3073 50  0000 C CNN
F 2 "" H 1975 2900 50  0001 C CNN
F 3 "" H 1975 2900 50  0001 C CNN
	1    1975 2900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F185EF3
P 1975 3300
AR Path="/5F185EF3" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F185EF3" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 1975 3050 50  0001 C CNN
F 1 "GND" H 1980 3127 50  0000 C CNN
F 2 "" H 1975 3300 50  0001 C CNN
F 3 "" H 1975 3300 50  0001 C CNN
	1    1975 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1875 2900 1975 2900
Wire Wire Line
	1950 3000 1875 3000
Wire Wire Line
	1875 3100 1950 3100
Wire Wire Line
	1875 3300 1975 3300
NoConn ~ 1875 3200
Text Notes 2175 2800 0    50   ~ 0
5V 2A
$Comp
L Device:C C?
U 1 1 5F1875FF
P 3750 3075
AR Path="/5F1875FF" Ref="C?"  Part="1" 
AR Path="/5F183216/5F1875FF" Ref="C27"  Part="1" 
F 0 "C27" H 3865 3121 50  0000 L CNN
F 1 "0.1uF" H 3850 2950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3788 2925 50  0001 C CNN
F 3 "~" H 3750 3075 50  0001 C CNN
F 4 "1276-1043-1-ND" H 3750 3075 50  0001 C CNN "Digi-Key Part Number"
	1    3750 3075
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F187605
P 4150 3075
AR Path="/5F187605" Ref="C?"  Part="1" 
AR Path="/5F183216/5F187605" Ref="C28"  Part="1" 
F 0 "C28" H 4265 3121 50  0000 L CNN
F 1 "1uF" H 4250 2950 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4188 2925 50  0001 C CNN
F 3 "~" H 4150 3075 50  0001 C CNN
F 4 "1276-1102-1-ND" H 4150 3075 50  0001 C CNN "Digi-Key Part Number"
	1    4150 3075
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F18760B
P 3950 2875
AR Path="/5F18760B" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F18760B" Ref="#PWR040"  Part="1" 
F 0 "#PWR040" H 3950 2725 50  0001 C CNN
F 1 "+5V" H 3965 3048 50  0000 C CNN
F 2 "" H 3950 2875 50  0001 C CNN
F 3 "" H 3950 2875 50  0001 C CNN
	1    3950 2875
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2925 3750 2875
Wire Wire Line
	3750 2875 3950 2875
Wire Wire Line
	4150 2925 4150 2875
Wire Wire Line
	4150 2875 3950 2875
Connection ~ 3950 2875
$Comp
L power:GND #PWR?
U 1 1 5F187616
P 3950 3325
AR Path="/5F187616" Ref="#PWR?"  Part="1" 
AR Path="/5F183216/5F187616" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 3950 3075 50  0001 C CNN
F 1 "GND" H 3955 3152 50  0000 C CNN
F 2 "" H 3950 3325 50  0001 C CNN
F 3 "" H 3950 3325 50  0001 C CNN
	1    3950 3325
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3225 4150 3325
Wire Wire Line
	4150 3325 3950 3325
Wire Wire Line
	3750 3225 3750 3325
Wire Wire Line
	3750 3325 3950 3325
Connection ~ 3950 3325
Text Notes 3775 2600 0    50   ~ 0
USB VBus
$EndSCHEMATC
