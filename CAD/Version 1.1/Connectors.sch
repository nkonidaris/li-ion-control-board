EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x06_Male J?
U 1 1 5F182C92
P 1475 3925
AR Path="/5F182C92" Ref="J?"  Part="1" 
AR Path="/5F17A694/5F182C92" Ref="J2"  Part="1" 
F 0 "J2" H 1583 4306 50  0000 C CNN
F 1 "Conn_01x06_Male" H 1583 4215 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Horizontal" H 1475 3925 50  0001 C CNN
F 3 "~" H 1475 3925 50  0001 C CNN
F 4 "S1111EC-06-ND" H 1475 3925 50  0001 C CNN "Digi-Key Part Number"
	1    1475 3925
	1    0    0    -1  
$EndComp
Text GLabel 1675 3925 2    50   Input ~ 0
AC_Neutral_C
$Comp
L power:+5V #PWR?
U 1 1 5F182C9C
P 2325 3600
AR Path="/5F182C9C" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F182C9C" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 2325 3450 50  0001 C CNN
F 1 "+5V" H 2340 3773 50  0000 C CNN
F 2 "" H 2325 3600 50  0001 C CNN
F 3 "" H 2325 3600 50  0001 C CNN
	1    2325 3600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F182CA2
P 2325 3900
AR Path="/5F182CA2" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F182CA2" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 2325 3650 50  0001 C CNN
F 1 "GND" H 2330 3727 50  0000 C CNN
F 2 "" H 2325 3900 50  0001 C CNN
F 3 "" H 2325 3900 50  0001 C CNN
	1    2325 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	1675 3725 2325 3725
Wire Wire Line
	2325 3725 2325 3600
Wire Wire Line
	2325 3825 1675 3825
Wire Wire Line
	2325 3825 2325 3900
Text GLabel 1675 4125 2    50   Input ~ 0
I2C_Data
Text GLabel 1675 4225 2    50   Input ~ 0
I2C_Clock
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F182CAE
P 4175 5150
AR Path="/5F182CAE" Ref="J?"  Part="1" 
AR Path="/5F17A694/5F182CAE" Ref="J5"  Part="1" 
F 0 "J5" H 4255 5192 50  0000 L CNN
F 1 "Output Voltage AC" H 4255 5101 50  0000 L CNN
F 2 "Li-ion_Thesis_Footprint_Library:1984976_HV_Terminal" H 4175 5150 50  0001 C CNN
F 3 "~" H 4175 5150 50  0001 C CNN
F 4 "277-6355-ND" H 4175 5150 50  0001 C CNN "Digi-Key Part Number"
	1    4175 5150
	1    0    0    -1  
$EndComp
Text GLabel 3425 3425 0    50   Input ~ 0
VRefLive
$Comp
L power:GND #PWR?
U 1 1 5F182CBB
P 2850 3275
AR Path="/5F182CBB" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F182CBB" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 2850 3025 50  0001 C CNN
F 1 "GND" H 2855 3102 50  0000 C CNN
F 2 "" H 2850 3275 50  0001 C CNN
F 3 "" H 2850 3275 50  0001 C CNN
	1    2850 3275
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F182CC1
P 3975 5250
AR Path="/5F182CC1" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F182CC1" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 3975 5000 50  0001 C CNN
F 1 "GND" H 3980 5077 50  0000 C CNN
F 2 "" H 3975 5250 50  0001 C CNN
F 3 "" H 3975 5250 50  0001 C CNN
	1    3975 5250
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_ARM_JTAG_SWD_10 J?
U 1 1 5F182CC7
P 3250 1625
AR Path="/5F182CC7" Ref="J?"  Part="1" 
AR Path="/5F17A694/5F182CC7" Ref="J3"  Part="1" 
F 0 "J3" H 2807 1671 50  0000 R CNN
F 1 "Conn_ARM_JTAG_SWD_10" H 2807 1580 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 3250 1625 50  0001 C CNN
F 3 "http://infocenter.arm.com/help/topic/com.arm.doc.ddi0314h/DDI0314H_coresight_components_trm.pdf" V 2900 375 50  0001 C CNN
F 4 "2057-PH1-12-UA-ND" H 3250 1625 50  0001 C CNN "Digi-Key Part Number"
	1    3250 1625
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F182CCD
P 3250 1025
AR Path="/5F182CCD" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F182CCD" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 3250 875 50  0001 C CNN
F 1 "+3.3V" H 3265 1198 50  0000 C CNN
F 2 "" H 3250 1025 50  0001 C CNN
F 3 "" H 3250 1025 50  0001 C CNN
	1    3250 1025
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F182CD3
P 3200 2300
AR Path="/5F182CD3" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F182CD3" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 3200 2050 50  0001 C CNN
F 1 "GND" H 3205 2127 50  0000 C CNN
F 2 "" H 3200 2300 50  0001 C CNN
F 3 "" H 3200 2300 50  0001 C CNN
	1    3200 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 2225 3150 2250
Wire Wire Line
	3150 2250 3200 2250
Wire Wire Line
	3250 2250 3250 2225
Wire Wire Line
	3200 2300 3200 2250
Connection ~ 3200 2250
Wire Wire Line
	3200 2250 3250 2250
Text GLabel 3750 1325 2    50   Input ~ 0
NRST
Text GLabel 3750 1525 2    50   Input ~ 0
SWDCLK
Text GLabel 3750 1625 2    50   Input ~ 0
SWDIO
Text GLabel 3750 1725 2    50   Input ~ 0
SWO
NoConn ~ 3750 1825
$Comp
L Li-ion_Thesis_Symbol_Library:ACS70331EESATR-005B3 IC1
U 1 1 5F18CA01
P 1275 5675
F 0 "IC1" H 1125 6325 50  0000 C CNN
F 1 "ACS70331EESATR-005B3" H 1075 6200 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Allegro_QFN-12-10-1EP_3x3mm_P0.5mm" H 2325 6075 50  0001 L CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/ACS70331-Datasheet.ashx?la=en&hash=6ED550430D2A1B2B4764E95E08812EDF083CCD64" H 2325 5975 50  0001 L CNN
F 4 "Allegro Microsystems" H 2375 5975 50  0001 L CNN "Manufacturer_Name"
F 5 "ACS70331EESATR-005B3" H 2425 5225 50  0001 L CNN "Manufacturer_Part_Number"
F 6 "620-1887-1-ND" H 1275 5675 50  0001 C CNN "Digi-Key Part Number"
F 7 "2.728" H 1275 5675 50  0001 C CNN "Cost (A$)"
	1    1275 5675
	1    0    0    -1  
$EndComp
Text GLabel 1675 4025 2    50   Input ~ 0
AC_Neutral_C
Text GLabel 1275 5675 0    50   Input ~ 0
AC_Neutral_C
$Comp
L power:+3.3V #PWR?
U 1 1 5F18DF5D
P 1775 5075
AR Path="/5F18DF5D" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F18DF5D" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 1775 4925 50  0001 C CNN
F 1 "+3.3V" H 1725 5250 50  0000 C CNN
F 2 "" H 1775 5075 50  0001 C CNN
F 3 "" H 1775 5075 50  0001 C CNN
	1    1775 5075
	1    0    0    -1  
$EndComp
Text GLabel 2475 5675 2    50   Input ~ 0
VCurrent
$Comp
L power:GND #PWR?
U 1 1 5F18E44D
P 1825 6650
AR Path="/5F18E44D" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F18E44D" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 1825 6400 50  0001 C CNN
F 1 "GND" H 1830 6477 50  0000 C CNN
F 2 "" H 1825 6650 50  0001 C CNN
F 3 "" H 1825 6650 50  0001 C CNN
	1    1825 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	1775 6575 1775 6625
Wire Wire Line
	1775 6625 1825 6625
Wire Wire Line
	1875 6625 1875 6575
Wire Wire Line
	1825 6650 1825 6625
Connection ~ 1825 6625
Wire Wire Line
	1825 6625 1875 6625
$Comp
L power:GND #PWR?
U 1 1 5F18EC35
P 2675 5950
AR Path="/5F18EC35" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F18EC35" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 2675 5700 50  0001 C CNN
F 1 "GND" H 2680 5777 50  0000 C CNN
F 2 "" H 2675 5950 50  0001 C CNN
F 3 "" H 2675 5950 50  0001 C CNN
	1    2675 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F18F17E
P 2100 5000
AR Path="/5F18F17E" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F18F17E" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 2100 4750 50  0001 C CNN
F 1 "GND" H 2105 4827 50  0000 C CNN
F 2 "" H 2100 5000 50  0001 C CNN
F 3 "" H 2100 5000 50  0001 C CNN
	1    2100 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	2475 5775 2675 5775
Wire Wire Line
	2675 5775 2675 5875
Wire Wire Line
	2475 5875 2675 5875
Connection ~ 2675 5875
Wire Wire Line
	2675 5875 2675 5950
Wire Wire Line
	1875 5075 1875 4925
Wire Wire Line
	1875 4925 2100 4925
Wire Wire Line
	2100 4925 2100 5000
$Comp
L Connector:Screw_Terminal_01x03 J?
U 1 1 5F19013F
P 3625 3325
AR Path="/5F19013F" Ref="J?"  Part="1" 
AR Path="/5F17A694/5F19013F" Ref="J4"  Part="1" 
F 0 "J4" H 3705 3367 50  0000 L CNN
F 1 "Input Voltage AC" H 3705 3276 50  0000 L CNN
F 2 "Li-ion_Thesis_Footprint_Library:1984976_HV_Terminal" H 3600 3550 50  0001 C CNN
F 3 "~" H 3625 3325 50  0001 C CNN
F 4 "277-6355-ND" H 3625 3325 50  0001 C CNN "Digi-Key Part Number"
	1    3625 3325
	1    0    0    -1  
$EndComp
Text Notes 3275 3050 0    50   ~ 0
220V 50Hz AC
Text GLabel 3975 5050 0    50   Input ~ 0
AC_Feedback
Text GLabel 3975 5150 0    50   Input ~ 0
AC_Feedback
$Comp
L power:GND #PWR?
U 1 1 5F24653D
P 1275 5975
AR Path="/5F24653D" Ref="#PWR?"  Part="1" 
AR Path="/5F17A694/5F24653D" Ref="#PWR042"  Part="1" 
F 0 "#PWR042" H 1275 5725 50  0001 C CNN
F 1 "GND" H 1280 5802 50  0000 C CNN
F 2 "" H 1275 5975 50  0001 C CNN
F 3 "" H 1275 5975 50  0001 C CNN
	1    1275 5975
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3225 2850 3275
Wire Wire Line
	2850 3225 3250 3225
Wire Wire Line
	3425 3325 3250 3325
Wire Wire Line
	3250 3325 3250 3225
Connection ~ 3250 3225
Wire Wire Line
	3250 3225 3425 3225
$EndSCHEMATC
