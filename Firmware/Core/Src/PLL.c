/*
 * PLL.c
 *
 *  Created on: Feb 12, 2021
 *      Author: hp
 */
#include "stdint.h"
#include <stdbool.h>
#include "PLL.h"
#include "gpio.h"
#include "math.h"
#include "Curnt_cont.h"
#include "modulator.h"

static float Sin_Loop_Count = 0;
/* Store vgrid voltage data */
float Store_gridVoltage(){
	//float Voltage_grid = ((ADC_Samples[0]-1840.0f)/4095.0f)* VBATT * NoModules *3.3f;
	float Voltage_grid = 4.0f * 4.2f * sinf(2.0f * M_PI * 50.0f * Sin_Loop_Count);
	Sin_Loop_Count += 5e-5;
			if (Sin_Loop_Count >= 0.02) {
				Sin_Loop_Count = 0;
			}
	return Voltage_grid;
	}

uint8_t count1 =0;
float V_q(){
	float Voltage_grid_array[20];
			for (uint8_t i= 0; i<20;i++){
			Voltage_grid_array[i]=Store_gridVoltage();}
	uint8_t L_count1=count1;
	uint8_t phaseshift1=10;
	if (count1+phaseshift1>=20) {
		phaseshift1-=(20-count1);
		L_count1=0;
	}
	//float Voltage_grid_array[20] = Voltage_grid_array();
	float V_alpha = Voltage_grid_array[L_count1+phaseshift1];
	count1++;
	if (count1>=20){
		count1=0;
	}
    float V_beta = Store_gridVoltage();
	/*float V_d = (A[0][0]*V_alpha) + (A[0][1]*V_beta);*/
    float s_o = sinf(tot_omega());
    float c_o = cosf(tot_omega());
	float V_q = (s_o*V_alpha) + (c_o*V_beta);
	return V_q;
}

float Vq_error(){
	float Vq_error =vq_ref-V_q();
	return Vq_error;
}

uint8_t count2 =0;
float Vq_error_previous(){
	float Vq_error_array[2];
	for (count2=0; count2<2;count2++){
			Vq_error_array[count2]=Vq_error();}
	uint8_t L_count2=count2;
		uint8_t phaseshift2=1;
		if (count2+phaseshift2>=2) {
			phaseshift2-=(2-count2);
			L_count2=0;
		}
	float Vq_error_previous = Vq_error_array[L_count2+phaseshift2];
	count2++;
		if (count2>=2){
			count2=0;
		}
	return Vq_error_previous;
}


float integral(){
	float integral = (Vq_error() + Vq_error_previous())*0.5*5e-5;
	return integral;
}

uint8_t count3 =0;
float integral_previous(){
	float integral_array[2];
		for (count3=0; count3<2;count3++){
			integral_array[count3]=integral();}
		uint8_t L_count3=count3;
				uint8_t phaseshift3=1;
				if (count3+phaseshift3>=2) {
					phaseshift3-=(2-count3);
					L_count3=0;
				}
			float integral_previous = integral_array[L_count3+phaseshift3];
			count3++;
				if (count3>=2){
					count3=0;
				}
	return integral_previous;
}


float tot_Integral(){
	float tot_Integral= integral_previous()+integral();
	return tot_Integral;
}

float PI(){
	float PI = (Vq_error()*kp_PLL) + (tot_Integral()*ki_PLL);
    return PI;
}

uint8_t count4 =0;
float PI_previous(){
	float PI_array[2];
	for (count4=0; count4<2;count4++){
				PI_array[count3]=PI();}
			uint8_t L_count4=count4;
					uint8_t phaseshift4=1;
					if (count4+phaseshift4>=2) {
						phaseshift4-=(2-count4);
						L_count4=0;
					}
				float PI_previous = PI_array[L_count4+phaseshift4];
				count4++;
					if (count4>=2){
						count4=0;
					}
		return PI_previous;
}

float omega(){
	float omega = (PI()+PI_previous())*0.5f*5e-5f;
	return omega;
}

uint8_t count5 =0;
float omega_previous(){
	float omega_array[2];
		for (count5=0; count5<2;count5++){
			omega_array[count5]=omega();}
				uint8_t L_count5=count5;
						uint8_t phaseshift5=1;
						if (count5+phaseshift5>=2) {
							phaseshift5-=(2-count5);
							L_count5=0;
						}
					float omega_previous = omega_array[L_count5+phaseshift5];
					count5++;
						if (count5>=2){
							count5=0;
						}
			return omega_previous;
}

float tot_omega(){
	float tot_omega= omega_previous()+omega();
	return tot_omega;
}

float active(){
	float active= cosf(tot_omega());
	return active;
}













