/*
 * modulator.c
 *
 *  Created on: 10 Sep 2020
 *      Author: NicholasKonidaris
 */

#include "modulator.h"
#include "gpio.h"
#include "math.h"
#include "tables.h"
#include "Curnt_cont.h"
#include "PLL.h"

/* Triangle Wave Lookup Table (41 Values at 50us sample rate) */
const float CarryWave[40] = { -1, -0.9, -0.8, -0.7, -0.6, -0.5, -0.4, -0.3,
		-0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1, 0.9, 0.8,
		0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0, -0.1, -0.2, -0.3, -0.4, -0.5,
		-0.6, -0.7, -0.8, -0.9 };

static uint8_t CarryWaveCount = 0;
static float Sin_Loop_Count = 0;

uint8_t SchedualerState=0;


uint8_t SwitchUpdateModulator() {

	float ReferenceVoltage = V_ref();

	//float ReferenceVoltage = (ADC_Samples[0] - 1840.0f) / 4095.0f * VBATT * NoModules * 3.0f;
	//float ReferenceVoltage = 4 * 4.2f * sinf(2 * M_PI * 50 * Sin_Loop_Count);

	//float ReferenceVoltage = 0;

	uint8_t SwitchUpdate = 0;
	uint8_t StateSelect = 4;

	for (int IntConnectNo = 0; IntConnectNo <= 3; IntConnectNo++) {

		uint8_t L_CarryWaveCount = CarryWaveCount;

		uint8_t phaseshift = (IntConnectNo * 5);
		if (CarryWaveCount + phaseshift >= 40) {
			phaseshift -= (40 - CarryWaveCount);
			L_CarryWaveCount = 0;
		}

		float Ck = 4 * 4.2 * CarryWave[L_CarryWaveCount + phaseshift];

		if (ReferenceVoltage > Ck && ReferenceVoltage >= -Ck) {
			//SwitchUpdate ^= 1UL << (7 - 2 * IntConnectNo);
			//SwitchUpdate ^= 1UL << (6 - 2 * IntConnectNo);
			StateSelect++;
		} else if (ReferenceVoltage < -Ck && ReferenceVoltage <= Ck) {
			//SwitchUpdate ^= 1UL << (7 - 2 * IntConnectNo);
			//SwitchUpdate ^= 0UL << (6 - 2 * IntConnectNo);
			StateSelect--;
		}
/*
		 else if (ReferenceVoltage <= Ck && ReferenceVoltage > -Ck) {
			SwitchUpdate ^= 0UL << (7 - 2 * IntConnectNo);
			SwitchUpdate ^= 1UL << (6 - 2 * IntConnectNo);
		} else if (ReferenceVoltage <= -Ck && ReferenceVoltage > Ck) {
			SwitchUpdate ^= 0UL << (7 - 2 * IntConnectNo);
			SwitchUpdate ^= 0UL << (6 - 2 * IntConnectNo);
		}
*/


		SwitchUpdate = Sequence[SchedualerState][StateSelect];

	}

	Sin_Loop_Count += 5e-5;
		if (Sin_Loop_Count >= 0.02) {
			Sin_Loop_Count = 0;
		}

	CarryWaveCount++;
	if (CarryWaveCount >= 40){
		CarryWaveCount = 0;
	}


	return SwitchUpdate;

}
