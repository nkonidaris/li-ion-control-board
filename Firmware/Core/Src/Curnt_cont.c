/*
 * Curnt_cont.c
 *
 *  Created on: Feb 12, 2021
 *      Author: hp
 */

#include "Curnt_cont.h"
#include "PLL.h"
#include "gpio.h"
#include "math.h"
#include "modulator.h"

static float Sin_Loop_Count = 0;
/*Store Iconv current data*/
float Store_Iconv(){
	//float Current_inv= ((ADC_Samples[2]-1840.0f)/4095.0f)*3.3f;
	float Current_inv = 1.6f * sinf(2 * M_PI * 50 * Sin_Loop_Count);
		Sin_Loop_Count += 5e-5f;
				if (Sin_Loop_Count >= 0.02) {
					Sin_Loop_Count = 0;
				}
	return Current_inv;
	}

float Iconv_error(){
	float Iconv_error = (active()*1.6)-Store_Iconv();
	return Iconv_error;
}

float Out_p(){
	float Out_p = Iconv_error()*kp_PR;
	return Out_p;
}

float in_pr1(){
	float in_pr1=(Iconv_error()-tot_out_pr2())*kr1_PR;
	return in_pr1;
}

uint8_t count6 =0;
float in_pr1_previous(){
	float in_pr1_array[2];
	for (count6=0; count6<2;count6++){
	in_pr1_array[count6]=in_pr1();}
	uint8_t L_count6=count6;
	uint8_t phaseshift6=1;
		if (count6+phaseshift6>=2) {
		phaseshift6-=(2-count6);
		L_count6=0;
		}
	float in_pr1_previous = in_pr1_array[L_count6+phaseshift6];
	count6++;
		if (count6>=2){
		count6=0;
		}
	return in_pr1_previous;
}

float Out_pr1(){
	float Out_pr1=(in_pr1()+in_pr1_previous())*0.5*5e-5;
	return Out_pr1;
}

uint8_t count7 =0;
float Out_pr1_previous(){
	float Out_pr1_array[2];
		for (count7=0; count7<2;count7++){
			Out_pr1_array[count7]=Out_pr1();}
		uint8_t L_count7=count7;
		uint8_t phaseshift7=1;
			if (count7+phaseshift7>=2) {
			phaseshift7-=(2-count7);
			L_count7=0;
			}
		float Out_pr1_previous = Out_pr1_array[L_count7+phaseshift7];
		count7++;
			if (count7>=2){
			count7=0;
			}
		return Out_pr1_previous;
}

float in_pr2(){
	float in_pr2=Out_pr1()+Out_pr1_previous();
	return in_pr2;
}

uint8_t count8 =0;
float in_pr2_previous(){
	float in_pr2_array[2];
			for (count8=0; count8<2;count8++){
				in_pr2_array[count8]=in_pr2();}
			uint8_t L_count8=count8;
			uint8_t phaseshift8=1;
				if (count8+phaseshift8>=2) {
				phaseshift8-=(2-count8);
				L_count8=0;
				}
			float in_pr2_previous = in_pr2_array[L_count8+phaseshift8];
			count8++;
				if (count8>=2){
				count8=0;
				}
			return in_pr2_previous;
}

float out_pr2(){
	float out_pr2 = (in_pr2()+in_pr2_previous())*0.5*5e-5;
	return out_pr2;
}

uint8_t count9 =0;
float out_pr2_previous(){
	float out_pr2_array[2];
	for (count9=0; count9<2;count9++){
	out_pr2_array[count9]=out_pr2();}
	uint8_t L_count9=count9;
	uint8_t phaseshift9=1;
		if (count8+phaseshift9>=2) {
		phaseshift9-=(2-count9);
		L_count9=0;
		}
	float out_pr2_previous = out_pr2_array[L_count9+phaseshift9];
	count9++;
		if (count9>=2){
		count9=0;
		}
	return out_pr2_previous;
}

float tot_out_pr2(){
	float tot_out_pr2=(out_pr2_previous()+out_pr2())*kr2_PR;
	return tot_out_pr2;
}

float V_ref(){
	float V_ref = (Out_p()+in_pr2()+Store_gridVoltage())/42.0f;

	return V_ref;
}














