/*
 * tables.c
 *
 *  Created on: Dec 8, 2020
 *      Author: hp
 */
#include "stdint.h"
#include "tables.h"


const uint8_t Sequence[5][9] = {{0b10101010, 0b10101000, 0b10100000,0b10000000,0b01010101,0b11010101,0b11110101,0b11111101,0b11111111},
								{0b10101010, 0b10001010,0b10100000,0b10000000,0b01010101,0b11010101,0b11110101,0b11011111,0b11111111},
								{0b10101010, 0b00101010,0b10100000,0b10000000,0b01010101,0b11010101,0b11110101,0b01111111,0b11111111},
								{0b10101010, 0b10100010,0b10100000,0b10000000,0b01010101,0b11010101,0b11110101,0b11110111,0b11111111},
								{0b10101010,0b10101000,0b10001000,0b10000000,0b01010101,0b11010101,0b11011101,0b11111101,0b11111111}};







/*
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
{},
};*/

