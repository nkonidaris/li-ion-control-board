/*
 * Curnt_cont.h
 *
 *  Created on: Feb 10, 2021
 *      Author: hp
 */

#define kp_PR 11.25f;
#define kr1_PR 247.5f;
#define kr2_PR 398.77f;

float Store_Iconv();
float  Iconv_error();
float   Out_p();
float in_pr1();
float Out_pr1();
float Out_pr1_array();
float Out_pr1_previous();
float in_pr2();
float out_pr2();
float out_pr2_array();
float out_pr2_previous();
float tot_out_pr2();
float V_ref();
