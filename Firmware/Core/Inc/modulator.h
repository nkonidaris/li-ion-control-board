/*
 * modulator.h
 *
 *  Created on: 10 Sep 2020
 *      Author: NicholasKonidaris
 */

#ifndef INC_MODULATOR_H_
#define INC_MODULATOR_H_

#include <stdint.h>
#include <stdbool.h>
#include "tables.h"

#define Vref 3.307f
#define VBATT 4.2f
#define NoModules 4


uint8_t SwitchUpdateModulator();

#endif /* INC_MODULATOR_H_ */
