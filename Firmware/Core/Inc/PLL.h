/*
 * PLL.h
 *
 *  Created on: Feb 10, 2021
 *      Author: hp
 */

#ifndef INC_PLL_H_
#define INC_PLL_H_

#include <stdint.h>
#include <stdbool.h>

#define vq_ref 0
#define kp_PLL 10
#define ki_PLL 600*0.25f
#define a Hello

float Store_gridVoltage();
float Voltage_grid_array();
float V_q();
void store_Vq_error();
float Vq_error_array();
float Vq_error_previous();
float integral();
float integral_array(uint8_t n);
float PI();
float omega();
float omega_array(uint8_t n);
float tot_omega();
float active();

#endif /* INC_PLL_H_ */
